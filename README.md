# RPG Characters
- - -
A java console application, where the user is able to create a character (one of four classes), 
can deal damage with the character, and equip armors and weapon on the character.
## Install
- - -
For the application to run the following software needs to be installed or downloaded.
+ Install JDK 17 
+ Install Intellij
+ Clone this repository
## Heroes and Items
- - - 
In this program, the user is able to create 4 types of Role Playing Game characters.

A hero can be:
+ Mage
+ Ranger
+ Rouge
+ Warrior

The heroes can equip weapons and armors. 

Weapons:
+ Axe
+ Bow
+ Dagger
+ Hammer
+ Staff
+ Sword
+ Wand

Armors:
+ Cloth
+ Leather
+ Mail
+ Plate

Each class of hero can only equip certain items:
+ **Mages** can equip **Staffs** and **Wands** for weapons, and **Cloth** for armor.
+ **Rangers** can equip **Bows** for weapon, and **Leather** and **Mail** for armor.
+ **Rouges** can equip **Daggers** and **Swords** for weapons, and **Leather** and **Mail** for armor.
+ **Warriors** can equip **Axes**, **Hammers** and **Swords** for weapons, and **Mail** and **Plate** for armor.


Each hero has **attributes**, their base attributes are automatically created.

They can also **level up**, on every level up their base attributes increase.

When a hero deals damage, their **Damage Per Second** is displayed, the DPS is calculated using the dps of their
equipped weapon and base attributes. Each class hase a **Main** attribute, that increase their damage.

The heroes Main attributes:
+ Mage: Intelligence
+ Ranger: Dexterity
+ Rouge: Dexterity
+ Warrior: Strength

## Unit testing
- - -
This project also includes unit testing for all major features. These test can be found in the **Tests** Directory.

## File Structure
- - -
>+ Assignment1
>  - .idea
>  - out
>  - src
>    * CustomErrors
>    * Heros
>    * Interfaces
>    * Items
>    * Main.java
>  - Tests
>    * HeroTests.java
>    * ItemTests.java
>  - .gitignore
>  - Asssignment1.iml
>  - README.md
