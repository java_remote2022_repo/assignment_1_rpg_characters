import CustomErrors.InvalidArmorException;
import CustomErrors.InvalidWeaponException;
import Heros.*;
import Items.Armor;
import Items.Enums;
import Items.Weapons;

public class Main extends Exception{
    public static void main(String[] args) {
        Warrior Garosh = new Warrior("Garosh");

        Weapons testWeapon = new Weapons("Common Axe",7,1.1,1, Enums.WeaponType.Axe);
        Armor testArmor = new Armor("Common Plate Body Armor",2, Enums.ArmorTypes.Plate,1,0,0);
        Garosh.stats();
        Garosh.levelUp();
        try{
            Garosh.equipItem(testWeapon,4);
            Garosh.equipItem(testArmor,1);
        }catch (InvalidWeaponException e){
            System.out.println(e);
        }catch (InvalidArmorException e){
            System.out.println(e);
        }

        Garosh.stats();
        Garosh.levelUp();
        Garosh.stats();

    }
}
