package Interfaces;

public interface DamageDealer {
    public double damage();
}
