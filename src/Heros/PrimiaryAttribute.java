package Heros;

public class PrimiaryAttribute {
    double Strength;
    double Dexterity;
    double Inteligence;

    public PrimiaryAttribute(){
        this.Strength = 0;
        this.Dexterity = 0;
        this.Inteligence = 0;
    }

    public PrimiaryAttribute(double s, double d, double i){
        this.Strength = s;
        this.Dexterity = d;
        this.Inteligence = i;
    }

    public void addExtraAtr(PrimiaryAttribute pa){      //adds a PrimaryAtributes values to the original's values
        this.Strength += pa.Strength;
        this.Dexterity += pa.Dexterity;
        this.Inteligence += pa.Inteligence;
    }

    public boolean isEquals(PrimiaryAttribute pa){      //returns true, if the given primary atribute has the same values as the original
        if(this.Strength == pa.Strength && this.Dexterity == pa.Dexterity && this.Inteligence == pa.Inteligence){
            return true;
        }else{
            return false;
        }

    }
}
