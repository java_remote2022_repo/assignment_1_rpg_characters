package Heros;

import CustomErrors.InvalidArmorException;
import CustomErrors.InvalidWeaponException;
import Interfaces.DamageDealer;
import Items.Armor;
import Items.Item;

import java.util.HashMap;


public abstract class Hero implements DamageDealer {
    String name;
    public int level;
    public PrimiaryAttribute baseAttributes = new PrimiaryAttribute();

    HashMap<Integer, Item> Equipment = new HashMap<Integer, Item>(); //id 0: Head equipment, id 1: body, id 2: Legs, id 4: Weapon


    public Hero(String n){
        this.name = n;
        this.level = 1;
    }

    public abstract void levelUp();

    //equips the given item to the given slot, weapons are equiped to slot 4
    //abstract because, every class has a different item that they can or cannot equip
    public abstract boolean equipItem(Item i, int slot) throws InvalidWeaponException, InvalidArmorException;


    public PrimiaryAttribute calculateTPA(){            //calculates the total primary atributes wich is base attributes + every armors extra attribute
        PrimiaryAttribute totalAttributes = new PrimiaryAttribute();
        totalAttributes.addExtraAtr(baseAttributes);
        for(int key: Equipment.keySet()){
            if(key != 4){
                Armor temp = (Armor) Equipment.get(key);
                totalAttributes.addExtraAtr(temp.extraAtr);
            }
        }
        return totalAttributes;
    }

    public void stats(){        //prints out all the statistics of a hero + there equipment
        System.out.println(name + " Level: " +level+
                ", Strength: "+ calculateTPA().Strength+
                ", Dexterity: " + calculateTPA().Dexterity +
                ", Inteligence: " + calculateTPA().Inteligence);
        damage();
        if(!Equipment.isEmpty()){
            System.out.println("Equipment:");
            for(Item i : Equipment.values()){
                System.out.println(i.Name);
            }
        }else{
            System.out.println(name + " has no equipment.");
        }
    }



}
