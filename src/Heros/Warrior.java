package Heros;

import CustomErrors.InvalidArmorException;
import CustomErrors.InvalidWeaponException;
import Items.Armor;
import Items.Enums;
import Items.Item;
import Items.Weapons;



public class Warrior extends Hero{
    public Warrior(String name){
        super(name);
        this.baseAttributes =new PrimiaryAttribute(5,2,1);
    }

    @Override
    public void levelUp() {
        this.level ++;
        this.baseAttributes.Strength += 3;
        this.baseAttributes.Dexterity += 2;
        this.baseAttributes.Inteligence += 1;
    }

    @Override
    public boolean equipItem(Item i, int slot) throws InvalidWeaponException,InvalidArmorException {
        if(slot == 4){
            Weapons temp = (Weapons) i;
            if(temp.ReqLevel > this.level){
                throw new InvalidWeaponException("Cannot equip this weapon, the hero's level is too low.");
            }
            if(temp.wp != Enums.WeaponType.Axe && temp.wp != Enums.WeaponType.Sword && temp.wp != Enums.WeaponType.Hammer){
                throw new InvalidWeaponException("This class cannot equip this type of weapon.");
            }
        }else{
            Armor tempArmor = (Armor) i;
            if(tempArmor.ReqLevel > this.level){
                throw new InvalidArmorException("Cannot equip this armor, the hero's level is too low.");
            }
            if(tempArmor.at != Enums.ArmorTypes.Mail && tempArmor.at != Enums.ArmorTypes.Plate)
                throw new InvalidArmorException("This class cannot equip this type of armor.");
        }

        Equipment.put(slot,i);
        return true;

    }

    @Override
    public double damage() {
        Weapons temp = (Weapons) Equipment.get(4);
        double dps;
        if(temp == null){
            dps = 1 * (1 + (calculateTPA().Strength / 100));;
        }else{
            dps = (temp.Damage * temp.APS) * (1 + (calculateTPA().Strength/100));
        }
        System.out.println(this.name + " deals "+ String.format("%.2f", dps) +" damage per second");
        return dps;
    }
}
