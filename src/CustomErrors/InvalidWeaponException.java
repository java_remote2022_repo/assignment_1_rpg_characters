package CustomErrors;

public class InvalidWeaponException extends Exception{
    public InvalidWeaponException(String message){
        super(message);
    }
}
