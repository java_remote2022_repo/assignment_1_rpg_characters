package Items;

public class Enums {
    public enum WeaponType {
        Axe,
        Bow,
        Dagger,
        Hammer,
        Staff,
        Sword,
        Wand,
    }

    public enum ArmorTypes {
        Cloth,
        Leather,
        Mail,
        Plate,
    }

}
