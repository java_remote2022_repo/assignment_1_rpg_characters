package Items;


public class Weapons extends Item{


    public Enums.WeaponType wp;
    public int Damage;
    public double APS;    //DPS = Damage * APS

    public Weapons(String name, int dam, double aps, int level, Enums.WeaponType wtype){
        super(name,level);
        this.wp = wtype;
        this.Damage = dam;
        this.APS = aps;
    }


}
