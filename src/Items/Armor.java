package Items;

import Heros.PrimiaryAttribute;

public class Armor extends Item{

    public PrimiaryAttribute extraAtr;
    public Enums.ArmorTypes at;

    public Armor(String name, int level, Enums.ArmorTypes type,int extraStrength, int extraDextarity, int extraInteligence){
        super(name,level);
        extraAtr = new PrimiaryAttribute(extraStrength,extraDextarity,extraInteligence);
        this.at = type;
    }


}
