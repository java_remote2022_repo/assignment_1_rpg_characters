

import Heros.*;
import org.junit.jupiter.api.Test;


import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;


class HeroTests {


    @Test
    public void Character_level1_when_created(){
        Warrior Garosh = new Warrior("Garosh");
        int expected = 1;
        int actual = Garosh.level;
        assertEquals(expected,actual);
    }

    @Test
    public void Character_level2_after_levelup(){
        Warrior Garosh = new Warrior("Garosh");
        Garosh.levelUp();
        int expected = 2;
        int actual = Garosh.level;
        assertEquals(expected,actual);
    }

    @Test
    public void Mage_attributes_correct_when_created(){
        Mage Jaina = new Mage("Jaina");
        PrimiaryAttribute expected = new PrimiaryAttribute(1,1,8);
        PrimiaryAttribute actual = Jaina.baseAttributes;
        assertTrue(expected.isEquals(actual));
    }

    @Test
    public void Ranger_attributes_correct_when_created(){
        Ranger Legolas = new Ranger("Legolas");
        PrimiaryAttribute expected = new PrimiaryAttribute(1,7,1);
        PrimiaryAttribute actual = Legolas.baseAttributes;
        assertTrue(expected.isEquals(actual));
    }

    @Test
    public void Rouge_attributes_correct_when_created(){
        Rouge Valeera = new Rouge("Valeera");
        PrimiaryAttribute expected = new PrimiaryAttribute(2,6,1);
        PrimiaryAttribute actual = Valeera.baseAttributes;
        assertTrue(expected.isEquals(actual));
    }

    @Test
    public void Warrior_attributes_correct_when_created(){
        Warrior Garosh = new Warrior("Garosh");
        PrimiaryAttribute expected = new PrimiaryAttribute(5,2,1);
        PrimiaryAttribute actual = Garosh.baseAttributes;
        assertTrue(expected.isEquals(actual));
    }

    @Test
    public void Mage_attributes_correct_after_levelup(){
        Mage Jaina = new Mage("Jaina");
        Jaina.levelUp();
        PrimiaryAttribute expected = new PrimiaryAttribute(2,2,13);
        PrimiaryAttribute actual = Jaina.baseAttributes;
        assertTrue(expected.isEquals(actual));
    }

    @Test
    public void Ranger_attributes_correct_after_levelup(){
        Ranger Legolas = new Ranger("Legolas");
        Legolas.levelUp();
        PrimiaryAttribute expected = new PrimiaryAttribute(2,12,2);
        PrimiaryAttribute actual = Legolas.baseAttributes;
        assertTrue(expected.isEquals(actual));
    }

    @Test
    public void Rouge_attributes_correct_after_levelup(){
        Rouge Valeera = new Rouge("Valeera");
        Valeera.levelUp();
        PrimiaryAttribute expected = new PrimiaryAttribute(3,10,2);
        PrimiaryAttribute actual = Valeera.baseAttributes;
        assertTrue(expected.isEquals(actual));
    }

    @Test
    public void Warrior_attributes_correct_after_levelup() {
        Warrior Garosh = new Warrior("Garosh");
        Garosh.levelUp();
        PrimiaryAttribute expected = new PrimiaryAttribute(8, 4, 2);
        PrimiaryAttribute actual = Garosh.baseAttributes;
        assertTrue(expected.isEquals(actual));
    }

}