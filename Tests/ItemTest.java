import static org.junit.jupiter.api.Assertions.*;

import CustomErrors.InvalidArmorException;
import CustomErrors.InvalidWeaponException;
import Heros.Hero;
import Heros.Warrior;
import Items.Armor;
import Items.Enums;
import Items.Weapons;
import org.junit.jupiter.api.Test;


import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

class ItemTest {
    //The items and equipment tests use a Warrior class for testing, an axe for an example weapon and plate body armor as
    //an example armor. We also use a bow for a wrong weapon type, and cloth head armor for wrong armor type.

    @Test
    public void Character_Should_not_be_able_to_equip_higher_level_weapon(){

        Warrior Garosh = new Warrior("Garosh");
        Weapons testWeapon = new Weapons("Common Axe",7,1.1,2, Enums.WeaponType.Axe);
        String expected = "Cannot equip this weapon, the hero's level is too low.";
        Exception exception = assertThrows(InvalidWeaponException.class,
                () -> Garosh.equipItem(testWeapon,4));
        String actual = exception.getMessage();
        assertEquals(expected,actual);
    }

    @Test
    public void Character_Should_not_be_able_to_equip_higher_level_armor(){
        Warrior Garosh = new Warrior("Garosh");
        Armor testArmor = new Armor("Common Plate Body Armor",2, Enums.ArmorTypes.Plate,1,0,0);
        String expected = "Cannot equip this armor, the hero's level is too low.";
        Exception exception = assertThrows(InvalidArmorException.class,
                () -> Garosh.equipItem(testArmor,1));
        String actual = exception.getMessage();
        assertEquals(expected,actual);
    }

    @Test
    public void Character_Should_not_be_able_to_equip_wrong_weapon(){
        Warrior Garosh = new Warrior("Garosh");
        Weapons testWrongWeapon = new Weapons("Common Bow",12,0.8,1, Enums.WeaponType.Bow);
        String expected = "This class cannot equip this type of weapon.";
        Exception exception = assertThrows(InvalidWeaponException.class,
                () -> Garosh.equipItem(testWrongWeapon,4));
        String actual = exception.getMessage();
        assertEquals(expected,actual);
    }

    @Test
    public void Character_Should_not_be_able_to_equip_wrong_armor(){
        Warrior Garosh = new Warrior("Garosh");
        Armor testWrongArmor = new Armor("Common Cloth Head Armor",1, Enums.ArmorTypes.Cloth,0,0,5);
        String expected = "This class cannot equip this type of armor.";
        Exception exception = assertThrows(InvalidArmorException.class,
                () -> Garosh.equipItem(testWrongArmor,1));
        String actual = exception.getMessage();
        assertEquals(expected,actual);
    }

    @Test
    public void Boolean_true_should_be_returned_when_valid_weapon_equipped(){
        Warrior Garosh = new Warrior("Garosh");
        Weapons testWeapon = new Weapons("Common Axe",7,1.1,1, Enums.WeaponType.Axe);
        boolean expected = true;
        boolean actual = false;
        try{
            actual =Garosh.equipItem(testWeapon,4);
        }catch (InvalidWeaponException e){
            System.out.println(e);
        }catch (InvalidArmorException e){
            System.out.println(e);
        }
        assertEquals(expected,actual);
    }

    @Test
    public void Boolean_true_should_be_returned_when_valid_armor_equipped(){
        Warrior Garosh = new Warrior("Garosh");
        Armor testArmor = new Armor("Common Plate Body Armor",1, Enums.ArmorTypes.Plate,1,0,0);
        boolean expected = true;
        boolean actual = false;
        try{
           actual =Garosh.equipItem(testArmor,1);
        }catch (InvalidWeaponException e){
            System.out.println(e);
        }catch (InvalidArmorException e){
            System.out.println(e);
        }
        assertEquals(expected,actual);
    }

    @Test
    public void Correct_dps_when_no_weapon_is_equipped(){
        Warrior Garosh = new Warrior("Garosh");
        double expected = 1 * ( 1 + (5.0 / 100));
        double actual = Garosh.damage();
        assertEquals(expected,actual);

    }

    @Test
    public void Correct_dps_when_valid_weapon_is_equipped(){
        Warrior Garosh = new Warrior("Garosh");
        Weapons testWeapon = new Weapons("Common Axe",7,1.1,1, Enums.WeaponType.Axe);
        try{
            Garosh.equipItem(testWeapon,4);
        }catch (InvalidWeaponException e){
            System.out.println(e);
        }catch (InvalidArmorException e){
            System.out.println(e);
        }
        double ecpected =  (7 * 1.1)*(1 + (5.0 / 100));
        double actual = Garosh.damage();
        assertEquals(ecpected,actual);

    }

    @Test
    public void Correct_dps_when_valid_weapon_and_valid_armor_equipped(){
        Warrior Garosh = new Warrior("Garosh");
        Weapons testWeapon = new Weapons("Common Axe",7,1.1,1, Enums.WeaponType.Axe);
        Armor testArmor = new Armor("Common Plate Body Armor",1, Enums.ArmorTypes.Plate,1,0,0);
        try{
            Garosh.equipItem(testWeapon,4);
            Garosh.equipItem(testArmor,1);
        }catch (InvalidWeaponException e){
            System.out.println(e);
        }catch (InvalidArmorException e){
            System.out.println(e);
        }
        double expected =  (7 * 1.1) * (1 + ((5.0+1) / 100));
        double actual = Garosh.damage();
        assertEquals(expected,actual);

    }
}